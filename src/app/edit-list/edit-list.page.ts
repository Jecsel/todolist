import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-edit-list',
  templateUrl: './edit-list.page.html',
  styleUrls: ['./edit-list.page.scss'],
})
export class EditListPage implements OnInit {
title: String;
description: String;
  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  update(id: String){
    this.http.put('http://localhost:3000/list/'+id, {list: {
      title: this.title,
      description: this.description,
      }
    }).subscribe((response) => {
      console.log(response);
    });
  }

}
