import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { AddListPage } from '../add-list/add-list.page';
import { HttpClientModule } from '@angular/common/http';
import { EditListPage } from '../edit-list/edit-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],
  declarations: [HomePage, AddListPage, EditListPage],
entryComponents:[AddListPage, EditListPage]
})
export class HomePageModule {}
