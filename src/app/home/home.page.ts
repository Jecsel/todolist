import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AddListPage } from '../add-list/add-list.page';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { EditListPage } from '../edit-list/edit-list.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  public listData: any = [];
  public responseData: any = [];
  constructor(private route: Router, 
              public modalController: ModalController,
              private http: HttpClient,) {

              }

  ngOnInit() {
    this.http.get('http://localhost:3000/list')
    .subscribe((response) => {
      this.responseData = response;
      for (let i = 0; i < this.responseData.length; i++) {
        this.listData.push(response[i]);
    }
    });
  }

  delete(id: String){
    this.http.delete('http://localhost:3000/list/'+ id)
    .pipe(map(res => res))
    .subscribe((response) => {
      this.ngOnInit();
    });
  }

  async addModal() {
    const modal = await this.modalController.create({
      component: AddListPage
    });
    modal.present();
  }

  async editModal(data: Object) {
    console.log(data)
    const modal = await this.modalController.create({
      component: EditListPage,
      componentProps: data
    });
    modal.present();
  }
}
