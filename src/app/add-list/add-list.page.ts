import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-add-list',
  templateUrl: './add-list.page.html',
  styleUrls: ['./add-list.page.scss'],
})
export class AddListPage implements OnInit {
  private title: string;
  private description: string;
  constructor(private route: Router,private http: HttpClient,public modalController: ModalController) { }

  ngOnInit() {
  }

  addlist() {
    this.http.post('http://localhost:3000/list', {list: {
      title: this.title,
      description: this.description,
      }
    }).subscribe((response) => {
      console.log(response);
    });
  }
 
  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }

}
